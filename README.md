# kits curso

 Introducción a la Programación y la Electrónica
Taller teórico-práctico orientado al público en general en donde intentaremos entender la tecnología a través de la creación de un proyecto electrónico DIY (do it yourself, o hazlo tu mismo) con Arduino.
En este taller teórico/práctico vas a aprender a usar la placa de desarrollo Arduino para materializar un proyecto. La mejor forma de aprender es haciendo, por eso propongo una metodología de taller en donde quien está aprendiendo desarrolla, de principio a fin, algún proyecto a elección, desde la elección de los sensores hasta la programación. La teoría y la práctica van de la mano, primero se entiende el porqué en la teoría y luego se afianza el conocimiento adquirido en la práctica. De esta manera el conocimiento queda. 

¿Qué aprenderemos en concreto?

- la tecnología
    - cómo funcionan las cosas?
    - téchnē, técnica, arte, oficio

- qué es Arduino?
    - cómo funciona Arduino?
    - modelos open-source, open-hard, copy-left

- Sistema Caja Negra: Input, Proceso, Output

- Conceptos de electricidad DC:
    - Volts, Amperes, Ohms
    - Polos, Multímetro (tester)
    - Almacenamiento

- Conceptos de electrónica
    - Resistencias, Capacitores, Diodos, Leds
    - Relays, Transistores, MOSFET

- sensores y actuadores
    - qué son? cómo funcionan?
    - ejemplos prácticos
    
- manos a la obra I: a programar
    - qué es programar?
    - analógico/digital
    - Arduino IDE, funcionamiento,funciones, sintaxis, librerías
    - manos a la obra II: a ensamblar y seguir programando
