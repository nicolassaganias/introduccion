const int pinSensor = A0;
const int bomba = 2;
const int umbral = 390;

void setup() {
  Serial.begin(9600);
  pinMode(pinSensor, INPUT);
  pinMode(bomba, OUTPUT);
  digitalWrite(bomba, LOW);

}

void loop() {

    int  sensor = analogRead(pinSensor);
    Serial.println(sensor);
  
    if (sensor < umbral) {
      //REGAR
      digitalWrite(bomba, HIGH);
    }
    else {
      digitalWrite(bomba, LOW);
    }
    
}
