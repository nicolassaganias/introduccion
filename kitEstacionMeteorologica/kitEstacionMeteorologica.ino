#include <TFT.h> //incluyo las librerias necesarias
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "DHT.h"

#define cs   10 // declaro los pines del display tft 1.8"
#define dc   9
#define rst  8

TFT TFTscreen = TFT(cs, dc, rst); //creo el objeto TFTscreen (nuestro display)

char tPrint[8]; // creo variables que utilizaré luego
char hPrint[8];

#define DHTTYPE DHT22 // declaraciones necesarias para el sensor 
#define DHTPIN 7 // pin del sensor

DHT dht(DHTPIN, DHTTYPE); //creo el objeto dht (nuestro sensor)

void setup() {

  Serial.begin(9600); //inicio puerto serial para poder ver datos en el monitor serie

  dht.begin(); //inicio el sensor

  TFTscreen.begin(); //inicio el display
  TFTscreen.background(0, 0, 0); //pinto el fondo de negro (el color es RGB por lo tanto 0,0,0 es negro y 255,255,255 es blanco)
  TFTscreen.setRotation(1); // roto la pantalla. posibilidades: 0,1,2 y 3. va rotando 90 grados el display

  TFTscreen.stroke(255, 255, 255); // declaro el color del contorno de las letras con las que escribiré
  TFTscreen.setTextSize(2); // declaro el tamaño de letra

}

void loop() { // 128px x 160px

  float t = dht.readTemperature(); //leo temperatura y la guardo en la variable "t"
  float h = dht.readHumidity(); // //leo humedad y la guardo en la variable "h"
  
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  // float to string. (float, width, presicion, string)
  dtostrf(t, 4, 1, tPrint);
  dtostrf(h, 4, 1, hPrint);
 
  TFTscreen.background(0, 0, 0); // pinto el fondo de negro antes de escribir para borrar los datos anteriores y así poder refrescar los datos cada vez

  TFTscreen.stroke(255, 255, 255); // vuelvo a decirle que las letras son blancas
  TFTscreen.setTextSize(2); // y el tamaño de las letras
                            //estas dos declaraciones no son necesarias ya que estan en el setup pero estan aqui por si quisieramos cambiar tamaño o color

  TFTscreen.text("Temperatura ", 5, 5); //imprimo en el display la palabra "temperatura" en la posicion x=5, y=5.
  TFTscreen.rect(0, 0, 160, 128); // dibujo un rectangulo blanco al rededor del display (solo por estética)
  TFTscreen.text(tPrint, 5, 25); // imprimo la temperatura
  TFTscreen.text("C ", 60, 25); // imprimo la letra "C" de Celsius

  TFTscreen.text("Humedad ", 5, 45); //imprimo la palabra humedad
  TFTscreen.text(hPrint, 5, 65); //imprimo el valor humedad
  TFTscreen.text("% ", 60, 65); //imprimo %

  delay(20000); // espero 20 segundos (20.000 milisegundos) para volver a tomar un nuevo valor y re-imprimir todo

}
